import os
import json
import sys
import argparse

parser = argparse.ArgumentParser(description='Peform WHOIS lookups on all RIRs')
parser.add_argument('-r','--registrar', required=False)
parser.add_argument('-q','--query', required=True)
args = vars(parser.parse_args())

if args['query'] == '':
    print("A query is requried")
    exit();

default_rirs=("whois.arin.net","whois.apnic.net","whois.lacnic.net","whois.ripe.net","whois.afrinic.net")
rirs=list()
if args['registrar'] != None:
    rirs.append(args['registrar'])
else:
    rirs = default_rirs

def field_name_normalizer(field):
    normalizer={'OrgName': 'orgname',
            'OrgId': 'organisation',
            'OriginAS': 'origin',
            'NetRange': 'inetnum',
            'NetName': 'netname'}
    if field in normalizer.keys():
        return normalizer[field].lower()
    else:
        return field.lower()

# Deduplicate the field names so that JSON doesn't get upset with duplicate keys.
def dupe_field_handler(fields,field_array):
    max_dedupe = 20
    existing_fields_list=[]
    deduped_candidates=[]
    # loop through the field_array and collect the keys from each existing field
    for existing_field in field_array:
        for key in existing_field.keys():
            existing_fields_list.append(key)
    for i in range(1,max_dedupe):
        if i == 1:
            deduped_candidates.append(fields[0])
        else:
            deduped_candidates.append(fields[0] + " (" + str(i) + ")")
    # loop throught the existing_field_list and the deduping candidates, override the field[0] element
    # with the first candidate that doesn't match an already existing field.
    matched=-1
    for existing_field in sorted(existing_fields_list):
        for j in range(0,max_dedupe - 1):
            if deduped_candidates[j] == existing_field:
                matched = j
        fields[0] = deduped_candidates[matched+1]
    return fields

# Convert whois output into JSON
def whois_to_dict(input):
    whois_objects = {'as-block': 'as_block',
            'as-set': 'as_set',
            'aut-num': 'aut_num',
            'domain': 'domain',
            'filter-set': 'filter_set',
            'inet6num': 'network',
            'inetnum': 'network',
            'inet-rtr': 'inet_rtr',
            'irt': 'irt',
            'key-cert': 'key_cert',
            'mntner': 'mntner',
            'organization': 'org',
            'organisation': 'org',
            'peering-set': 'peering_set',
            'person': 'poc',
            'role': 'role',
            'route': 'route',
            'route6': 'route6',
            'route-set': 'route_set',
            'rtr-set': 'rtr_set',
            'NetRange': 'network',
            'OrgAbuseHandle': 'poc',
            'OrgNOCHandle': 'poc',
            'OrgTechHandle': 'poc',
            'OrgRoutingHandle': 'poc',
            'OrgName': 'organization',
            'CustName': 'customer',
            'RTechHandle': 'poc',
            'netrange': 'network',
            'orgabusehandle': 'poc',
            'orgnochandle': 'poc',
            'orgtechhandle': 'poc',
            'orgroutinghandle': 'poc',
            'org_name': 'organization',
            'custname': 'customer',
            'rtechhandle': 'poc'}
    full_record={}
    field_array=[]
    record={}
    object={}
    sub_object={}
    counter=0
    object_name=""

    obj_counter=0
    for line in input.splitlines():
        if len(line) != 0:
            fields = list(map(lambda s: s.strip(), line.split(':',1)))
            if len(fields) > 1:
                if (object_name=="" and fields[0] in whois_objects.keys()):
                        object_name=whois_objects[fields[0]]
                        obj_counter+=1
                fields[0]=field_name_normalizer(fields[0])
                # Strip out any commas in the field values.
                fields[1]=fields[1].replace(","," ")
                if not fields[0] in sub_object.keys():
                    sub_object[fields[0]]=[]
                sub_object[fields[0]].append(fields[1])

        else:
            if len(object_name) != 0:
                if not object_name in object.keys():
                    object[object_name]=[]
                    obj_counter=0
                object[object_name].append(sub_object)
            sub_object={}
            object_name=""
            field={}
    return object

def whois_lookup(rir, query):
    if rir == "whois.arin.net":
        # This is needed because ARIN is a special snowflake that uses a different query format than the other RIRs :|
        query = "+ z " + query
    stream = os.popen('/usr/bin/whois -H -h ' + rir + ' ' + query + '| grep -v -e ^% -e ^#')
    output = stream.read()
    json = whois_to_dict(output)
    return json

accumulated_results=[]
output={}
output['query']=args['query']
for rir in rirs:
     results={}
     result = whois_lookup(rir,args['query'])
     short_rir=rir.split('.')[1]
     results['rir']=short_rir
     results['response']=result
     accumulated_results.append(results)
output['results']=accumulated_results
print(json.dumps(output,indent=2))
